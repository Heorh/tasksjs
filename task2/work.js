﻿function square() {
    var res = "";
    for (var i = 0; i < 50000; i++) {
        res += "<div class='square'> </div>";
    }
    return res;
}

function fill() {
    var main = $("#main").first();
    var sq = square;
    main.html(sq());
}


function getTextColor() {
    var color = $('[id = "color_value"]').first();
    var valueColor = color.attr("value");
    return valueColor;
}


$(document).ready(function () {

    fill();

    var isMouseDown = false;
    $(".square").on("mousedown", function (e) {
        isMouseDown = true;
    });

    $(".square").on("mouseup", function (e) {
        isMouseDown = false;
    });

  
    $(".square").on("mousemove", function (e) {

        if (isMouseDown)
            $(this).css("background-color", "#" + getTextColor()); 
    });
}
); 